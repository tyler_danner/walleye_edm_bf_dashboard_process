#Standard imports

import pandas as pd
import datetime
import subprocess
import json
import re
import sys
import os

# Aunisght Connection

from lib_aunsight.context import AunsightContext
from dslib.ioutils.aunsight_connector import AunsightConnector
from lib_aunsight.lib.util.Logger import Logger

log_lvl = os.getenv("LOG_LEVEL", "INFO")


token = os.environ["AU_CONTEXT_TOKEN"]

# token = "your_token"
au_context = AunsightContext(token=token)
au_context = au_context.set_context(organization='6efba1cc-94d0-4965-ae10-063868002181')
ac = AunsightConnector(au_context)


def get_logger(name="main"):
    stream_id = os.getenv("AU_LOGGER_STREAM")
    return Logger(context=au_context, stream=stream_id,
        stdout=True,
        name=name,
        level=log_lvl,
        throw_on_error=True)
    
logger = get_logger(__name__)

# Get current date - 2 days and format the date as per source dataset.

today = datetime.date.today()
date_list=[]
for i in range(2,3):
    last = datetime.timedelta(days=i)
    actual_date = (today - last)
    date_list.append(actual_date.strftime("%Y%m%d"))

logger.info("Date for which metrics are calculated",date_list[0])

#Logic for filtering datasets with name containing "BF_staging"

walleye_guid = '6efba1cc-94d0-4965-ae10-063868002181'
tlog_proj = au_context.organization(walleye_guid)

# Get ldjson dsets
daily_dsets = []
done=False
PAGE_SIZE = 50
skip=0

opts = {
    'skip': skip
    ,'limit': PAGE_SIZE
    ,'query':{
        'name__contains':"BF_staging"
    }
}


while not done:
    dset_page, page_info = tlog_proj.atlas_records(
        ['id','name']
        ,opts=opts
    )
    daily_dsets.extend(dset_page)

    # since there's a max allowed page size, we need to make sure we check page_info.limit to see what was actually used
    PAGE_SIZE=page_info['limit']
    done = page_info['count'] < PAGE_SIZE
    skip += PAGE_SIZE

    opts['skip'] = skip
    opts['limit'] = PAGE_SIZE

logger.info("Querying for records finished length of records object is :{}".format(len(daily_dsets)))

#filtering based on date, getting datasets and ids not containg SKU

final_list = []
for d in daily_dsets:
    if re.search(date_list[0],d.get("name")):
        temp = d.get("id")
        if 'SKU' not in d.get("name").split("_"):
            final_list.append((d.get("id"),d.get("name")))
        else:
            continue
logger.info("Filtering for datasets for date {} and no SKU datasets completed".format(date_list[0]))
logger.info("Filtering for datasets completed, length of final list {}".format(len(final_list)))

#segregating based on whirlpool , kitchen_aid and maytag , each one of those contains a 'JW' followed by a date

if len(final_list) > 30:
    logger.info("Prioritizing datasets ending with 'JW' followed by date")
    

records_KitchenAid = []
records_Maytag = []
records_Whirlpool = []

for data in final_list:
    if "KitchenAid" in data[1].split("_") and re.search('JW',data[1]):
        records_KitchenAid.append(data)
    elif "Whirlpool" in data[1].split("_") and re.search('JW',data[1]):
        records_Whirlpool.append(data)
    elif "Maytag" in data[1].split("_")and re.search('JW',data[1]):
        records_Maytag.append(data)

logger.info("Filtering for datasets for different brands completed")
logger.info("Length of list related to KitchenAid records is {}".format(len(records_KitchenAid)))
logger.info("Length of list related to Whirlpool records is {}".format(len(records_Whirlpool)))
logger.info("Length of list related to Maytag records is {}".format(len(records_Maytag)))

if len(records_KitchenAid) <10 or len(records_Whirlpool) < 10 or len(records_Maytag) < 10 :
     logger.info('Some datasets missing')
     sys.exit()
    #breakpoint


#Downloading Kitchen-Aid data and storing them in files named from substring of the original file name
#special case for _P_A

logger.info("Downloading KitchenAid data")
for data in records_KitchenAid:
    rec = au_context.atlas_record(data[0])
    
    #string formatting
    words_list = data[1].split("_")
    if 'P' in words_list:
        words_list_2 = words_list[2:7]
    else:
        words_list_2 = words_list[2:6]
    temp_string = "_".join(words_list_2)
    rec.download(target_stream=open(temp_string,"wb"))

logger.info("KitchenAid data downloaded")


# Read the files in corresponding dataframes

logger.info("Reading KitchenAid data to dataframes")
df = pd.read_csv('KitchenAid_All_Products_Shopper',sep=",",header= 0)
df['Segment'] = "All Products"

df1 = pd.read_csv('KitchenAid_CPG_Shopper_Orders',sep=",",header= 0)
df1['Segment'] = "CPG"
df2 = pd.read_csv('KitchenAid_CPG_Shopper_Visit',sep=",",header= 0)
df2['Segment'] = "CPG"

df3 = pd.read_csv('KitchenAid_MDA_Shopper_Orders',sep=",",header= 0)
df3['Segment'] = "MDA"
df4 = pd.read_csv('KitchenAid_MDA_Shopper_Visit',sep=",",header= 0)
df4['Segment'] = "MDA"

df5 = pd.read_csv('KitchenAid_P_A_Shopper_Orders',sep=",",header= 0)
df5['Segment'] = "PA"
df6 = pd.read_csv('KitchenAid_P_A_Shopper_Visit',sep=",",header= 0)
df6['Segment'] = "PA"

df7 = pd.read_csv('KitchenAid_SDA_Shopper_Orders',sep=",",header= 0)
df7['Segment'] = "SDA"
df8 = pd.read_csv('KitchenAid_SDA_Shopper_Visit',sep=",",header= 0)
df8['Segment'] = "SDA"

df9 = pd.read_csv('KitchenAid_Owners_Shopper_Visit',sep=",",header= 0)
df9['Segment'] = "Owners"

logger.info("KitchenAid data read to dataframes, they are ready for merging")


# Transformation related to visitors and orders, since both of them needed to be in same record

logger.info("Merging KitchenAid data to have orders and visits on the same rows")
if len(df7) > 0 and len(df8) > 0:
    result_df_SDA = pd.merge(df7,df8,on='Segment')
    result_df_SDA=result_df_SDA.drop(['Date_x'],axis=1)
    result_df_SDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_SDA
elif len(df7) > 0 :
    result_df_SDA = df7.copy()
else:
    result_df_SDA = df8.copy()
    

if len(df5) > 0 and len(df6) > 0:
    result_df_PA = pd.merge(df5,df6,on='Segment')
    result_df_PA=result_df_PA.drop(['Date_x'],axis=1)
    result_df_PA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_PA
elif len(df5) > 0 :
    result_df_PA = df5.copy()
else:
    result_df_PA = df6.copy()


if len(df1) > 0 and len(df2) > 0:
    result_df_CPG = pd.merge(df1,df2,on='Segment')
    result_df_CPG=result_df_CPG.drop(['Date_x'],axis=1)
    result_df_CPG.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_CPG
elif len(df1) > 0 :
    result_df_CPG = df1.copy()
else:
    result_df_CPG = df2.copy()
    

if len(df3) > 0 and len(df4) > 0:
    result_df_MDA = pd.merge(df3,df4,on='Segment')
    result_df_MDA=result_df_MDA.drop(['Date_x'],axis=1)
    result_df_MDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_MDA
elif len(df3) > 0 :
    result_df_MDA = df3.copy()
else:
    result_df_MDA = df4.copy()

logger.info("Merging of KitchenAid data completed")


#Merge all 

logger.info("Concating KitchenAid data for aggregation")
df_temp = pd.concat([df,result_df_CPG,result_df_SDA,result_df_PA,result_df_MDA,df9])

logger.info("Cleaning column names for KitchenAid data")
#Clean columns 
df_temp.columns = df_temp.columns.str.replace("-", "_")
df_temp.columns = df_temp.columns.str.replace("[()]", "_")
df_temp.columns= df_temp.columns.str.replace(" ", "_")

logger.info("Adding brand = KitchenAid")
# Add brand
df_temp['Brand'] = "KitchenAid"

logger.info("Creating master dataframe and adding KitchenAid data to it.")
# Copy to master dataset
master_df = df_temp.copy()


#Downloading Whirlpool data and storing them in files named from substring of the original file name
#special case for _P_A
logger.info("Downloading Whirlpool data")
for data in records_Whirlpool:
    rec = au_context.atlas_record(data[0]);
    
    #string formatting
    words_list = data[1].split("_")
    if 'P' in words_list:
        words_list_2 = words_list[2:7]
    else:
        words_list_2 = words_list[2:6]
    temp_string = "_".join(words_list_2)
    rec.download(target_stream=open(temp_string,"wb"))

logger.info("Whirlpool data downloaded")


# Read the files in corresponding dataframes
logger.info("Reading Whirlpool data to dataframes")

df = pd.read_csv('Whirlpool_All_Products_Shopper',sep=",",header= 0)
df['Segment'] = "All Products"

df1 = pd.read_csv('Whirlpool_CPG_Shopper_Orders',sep=",",header= 0)
df1['Segment'] = "CPG"
df2 = pd.read_csv('Whirlpool_CPG_Shopper_Visit',sep=",",header= 0)
df2['Segment'] = "CPG"

df3 = pd.read_csv('Whirlpool_MDA_Shopper_Orders',sep=",",header= 0)
df3['Segment'] = "MDA"
df4 = pd.read_csv('Whirlpool_MDA_Shopper_Visit',sep=",",header= 0)
df4['Segment'] = "MDA"

df5 = pd.read_csv('Whirlpool_P_A_Shopper_Orders',sep=",",header= 0)
df5['Segment'] = "PA"
df6 = pd.read_csv('Whirlpool_P_A_Shopper_Visit',sep=",",header= 0)
df6['Segment'] = "PA"

df7 = pd.read_csv('Whirlpool_SDA_Shopper_Orders',sep=",",header= 0)
df7['Segment'] = "SDA"
df8 = pd.read_csv('Whirlpool_SDA_Shopper_Visit',sep=",",header= 0)
df8['Segment'] = "SDA"

df9 = pd.read_csv('Whirlpool_Owners_Shopper_Visit',sep=",",header= 0)
df9['Segment'] = "Owners"

logger.info("Whilrpool data read to dataframes, they are ready for merging")


# Transformation related to visitors and orders, since both of them needed to be in same record

logger.info("Merging Whirlpool data to have orders and visits on the same rows")
if len(df7) > 0 and len(df8) > 0:
    result_df_SDA = pd.merge(df7,df8,on='Segment')
    result_df_SDA=result_df_SDA.drop(['Date_x'],axis=1)
    result_df_SDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_SDA
elif len(df7) > 0 :
    result_df_SDA = df7.copy()
else:
    result_df_SDA = df8.copy()
    

if len(df5) > 0 and len(df6) > 0:
    result_df_PA = pd.merge(df5,df6,on='Segment')
    result_df_PA=result_df_PA.drop(['Date_x'],axis=1)
    result_df_PA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_PA
elif len(df5) > 0 :
    result_df_PA = df5.copy()
else:
    result_df_PA = df6.copy()


if len(df1) > 0 and len(df2) > 0:
    result_df_CPG = pd.merge(df1,df2,on='Segment')
    result_df_CPG=result_df_CPG.drop(['Date_x'],axis=1)
    result_df_CPG.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_CPG
elif len(df1) > 0 :
    result_df_CPG = df1.copy()
else:
    result_df_CPG = df2.copy()
    

if len(df3) > 0 and len(df4) > 0:
    result_df_MDA = pd.merge(df3,df4,on='Segment')
    result_df_MDA=result_df_MDA.drop(['Date_x'],axis=1)
    result_df_MDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_MDA
elif len(df3) > 0 :
    result_df_MDA = df3.copy()
else:
    result_df_MDA = df4.copy()



#Merge all 

logger.info("Concating Whirlpool data for aggregation")
df_temp2 = pd.concat([df,result_df_CPG,result_df_SDA,result_df_PA,result_df_MDA,df9])


#Clean columns 
logger.info("Cleaning column names for Whirlpool data")

df_temp2.columns = df_temp2.columns.str.replace("-", "_")
df_temp2.columns = df_temp2.columns.str.replace("[()]", "_")
df_temp2.columns= df_temp2.columns.str.replace(" ", "_")


# Add brand
logger.info("Adding brand = Whirlpool")

df_temp2['Brand'] = "Whirlpool"


# Append to master dataset
logger.info("Appending Whirlpool data to master dataframe.")

master_df=master_df.append(df_temp2)


#Downloading Maytag data and storing them in files named from substring of the original file name
#special case for _P_A
logger.info("Downloading Maytag data")
for data in records_Maytag:
    rec = au_context.atlas_record(data[0]);
    
    #string formatting
    words_list = data[1].split("_")
    if 'P' in words_list:
        words_list_2 = words_list[2:7]
    else:
        words_list_2 = words_list[2:6]
    temp_string = "_".join(words_list_2)
    rec.download(target_stream=open(temp_string,"wb"))

logger.info("Maytag data downloaded")

# Read the files in corresponding dataframes

logger.info("Reading Maytag data to dataframes")

df = pd.read_csv('Maytag_All_Products_Shopper',sep=",",header= 0)
df['Segment'] = "All Products"

df1 = pd.read_csv('Maytag_CPG_Shopper_Orders',sep=",",header= 0)
df1['Segment'] = "CPG"
df2 = pd.read_csv('Maytag_CPG_Shopper_Visit',sep=",",header= 0)
df2['Segment'] = "CPG"

df3 = pd.read_csv('Maytag_MDA_Shopper_Orders',sep=",",header= 0)
df3['Segment'] = "MDA"
df4 = pd.read_csv('Maytag_MDA_Shopper_Visit',sep=",",header= 0)
df4['Segment'] = "MDA"

df5 = pd.read_csv('Maytag_P_A_Shopper_Orders',sep=",",header= 0)
df5['Segment'] = "PA"
df6 = pd.read_csv('Maytag_P_A_Shopper_Visit',sep=",",header= 0)
df6['Segment'] = "PA"

df7 = pd.read_csv('Maytag_SDA_Shopper_Orders',sep=",",header= 0)
df7['Segment'] = "SDA"
df8 = pd.read_csv('Maytag_SDA_Shopper_Visit',sep=",",header= 0)
df8['Segment'] = "SDA"

df9 = pd.read_csv('Maytag_Owners_Shopper_Visit',sep=",",header= 0)
df9['Segment'] = "Owners"

logger.info("Maytag data read to dataframes, they are ready for merging")

# Transformation related to visitors and orders, since both of them needed to be in same record

logger.info("Merging Maytag data to have orders and visits on the same rows")

if len(df7) > 0 and len(df8) > 0:
    result_df_SDA = pd.merge(df7,df8,on='Segment')
    result_df_SDA=result_df_SDA.drop(['Date_x'],axis=1)
    result_df_SDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_SDA
elif len(df7) > 0 :
    result_df_SDA = df7.copy()
else:
    result_df_SDA = df8.copy()
    

if len(df5) > 0 and len(df6) > 0:
    result_df_PA = pd.merge(df5,df6,on='Segment')
    result_df_PA=result_df_PA.drop(['Date_x'],axis=1)
    result_df_PA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_PA
elif len(df5) > 0 :
    result_df_PA = df5.copy()
else:
    result_df_PA = df6.copy()


if len(df1) > 0 and len(df2) > 0:
    result_df_CPG = pd.merge(df1,df2,on='Segment')
    result_df_CPG=result_df_CPG.drop(['Date_x'],axis=1)
    result_df_CPG.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_CPG
elif len(df1) > 0 :
    result_df_CPG = df1.copy()
else:
    result_df_CPG = df2.copy()
    

if len(df3) > 0 and len(df4) > 0:
    result_df_MDA = pd.merge(df3,df4,on='Segment')
    result_df_MDA=result_df_MDA.drop(['Date_x'],axis=1)
    result_df_MDA.rename(columns={'Date_y':'Date'},inplace=True)
    result_df_MDA
elif len(df3) > 0 :
    result_df_MDA = df3.copy()
else:
    result_df_MDA = df4.copy()

#Merge all 

logger.info("Concating Maytag data for aggregation")
df_temp3 = pd.concat([df,result_df_CPG,result_df_SDA,result_df_PA,result_df_MDA,df9])


#Clean columns 
logger.info("Cleaning column names for Maytag data")
df_temp3.columns = df_temp3.columns.str.replace("-", "_")
df_temp3.columns = df_temp3.columns.str.replace("[()]", "_")
df_temp3.columns= df_temp3.columns.str.replace(" ", "_")

# Add brand
logger.info("Adding brand = Maytag")
df_temp3['Brand'] = "Maytag"

# Append to master dataset

logger.info("Appending Maytag data to master dataframe.")
master_df=master_df.append(df_temp3)


#Upload to aunsight dataset

if len(master_df) > 0:
    #sort columns
    master_df = master_df.reindex(sorted(master_df.columns), axis=1)
    
    if 'All_Visits' in list(master_df.columns):
        master_df = master_df.drop('All_Visits', 1)
    
    if 'Checkout_Step_5___Order_Confirmation__e10___Participation_' in list(master_df.columns):
        master_df = master_df.drop('Checkout_Step_5___Order_Confirmation__e10___Participation_', 1)
        
    ac.upload(master_df,"56b13a70-2cb6-48f6-8c27-46d3171c85dc", delimiter=",", write_mode='overwrite')
    logger.info("Success")
else:
    logger.info("Upload not succeeded")