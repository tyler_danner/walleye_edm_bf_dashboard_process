# Auto-generated code
import os
import logging
from lib_aunsight.context import AunsightContext
from lib_aunsight.lib.util.Logger import Logger

log_lvl = os.getenv("LOG_LEVEL", "INFO")
token = os.environ.get("AU_CONTEXT_TOKEN")
organization = os.environ.get("AU_ORGANIZATION")
context = AunsightContext(token=token)


def get_logger(name="main"):
    stream_id = os.getenv("AU_LOGGER_STREAM")
    return Logger(
        context=context,
        stream=stream_id,
        stdout=True,
        name=name,
        level=log_lvl,
        throw_on_error=True
    )
